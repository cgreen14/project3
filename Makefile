# DO NOT EDIT -------------------------------
C=gcc
CFLAGS=-Wall
CPP=g++
CPPFLAGS=-Wall
LD=g++
LDFLAGS=-lpthread
# # ---------------------------- END DO NOT EDIT /afs/nd.edu/user14/csesoft/new/bin/

CPPFLAGS += -std=c++11 -g   # Add your own flags here, or leave blank
LDFLAGS +=                  # Add your own flags here, or leave blank

all: mandel mandelmovie

mandel: mandel.o bitmap.o
	g++ mandel.o bitmap.o -o mandel -lpthread

mandel.o: mandel.cpp
	g++ -g -Wall -c mandel.cpp -o mandel.o

bitmap.o: bitmap.c
	gcc -Wall -g -c bitmap.c -o bitmap.o

mandelmovie: mandelmovie.cpp
	g++ -Wall -g mandelmovie.cpp -o mandelmovie

# Uncoment to use the C compiler
# %.o: %.c
# 	$(C) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f mandel mandelmovie *.o *.bmp
