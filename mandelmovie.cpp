#include "bitmap.h"
#include "mandel.h"
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>


using namespace std;


int main(int argc, char *argv[]) {
	if(argc != 2){
		cout << "Please Call Function in Form of \"./mandelmovie N\", where n is number of process running at one time\n";
		exit(1);
	}
	//define variables
	double procs = 50; //number of runs
	float xVal = -0.745;
	float yVal = 0;
	int n = atoi(argv[1]);
	double startScale = 2;
	double endScale = 0.0001;
	double valIter = (startScale - endScale)/procs;
	//cout << valIter << endl;
	float sValues[50] = {0};
	char *cmd = (char *)"./mandel";
	char * arguments[14];
	arguments[0] = (char *)"./mandel";
	arguments[1] = (char *)"-x";
	arguments[2] = (char *)to_string(xVal).c_str();
	arguments[3] = (char *)"-y";
	arguments[4] = (char *)to_string(yVal).c_str();
	arguments[5] = (char *)"-s";
	arguments[9] = (char*)"-m";
	arguments[10] = (char *)to_string(10000).c_str();
	arguments[11] = (char *)"-n";
	arguments[12] = (char *)to_string(4).c_str();
	arguments[11] = NULL;

//create array of "s" values to be inputed into exec range is: 2 - 0.0001
	for(int i = 0; i < procs; i++){
		sValues[i] = 2 - (float)i*(valIter);
		//cout << sValues[i] << endl;
	}
	int pTotal = 0;
	pid_t pid;
	int err = 0;
	//	Start First 5 Proccesses
	 for (int i = 0; i < n; i++) {
      pid = fork();
      if (pid == -1) {
        //fork didnt work
		cout << "issue with forking" << endl;
        exit(-1);
      }
      if (pid == 0) { //child process, need to run ./mandel -x 0.286932 -y 0.014287 -s sValues[n] -o mandel + n + .bmp
		arguments[6] = (char *)to_string(sValues[i]).c_str();
		arguments[7] = (char *)"-o";
		string outFile = "mandel" + to_string(i) + ".bmp";
		arguments[8] = (char *)outFile.c_str();
		//cout << cmd << " " << arguments[0] << arguments[1] << arguments[2] << arguments[3] << arguments[4] << arguments[5] << arguments[6] << arguments[7] << arguments[8] << endl;
		if(execvp(cmd,arguments) == -1){// throw them in the execvp with command at front arguements in array
				cout << "ERROR: execvp fail on attempt, double check your command and arguements" << " - " << getpid() << endl;
				fprintf(stderr, "child's execvp: %s\n", strerror(err));
				exit(-1);
		}
	  }
	  if (pid > 0) { //parent
		pTotal++;
		//cout << pTotal << " - " << pid << endl;
	  }
	 }
	 //cout << endl << endl << "made it out of first n" << endl << endl;
	 int children = n;
	 int status = 0;
	 //cout << "waiting for death..." << endl;
	 while(children < procs){	
		while((pid = waitpid(-1, &status, WNOHANG)) > 0){  //wait for a process to finish
			//cout << "a child has passed..." << endl;
			pid = fork();
			if (pid == -1){
				cout << "ERROR: Fork Failed" << endl;
				exit (-1);
			} if (pid == 0){ //child process	
				arguments[6] = (char *)to_string(sValues[children]).c_str();
				arguments[7] = (char *)"-o";
				string outFile = "mandel" + to_string(children) + ".bmp";
				arguments[8] = (char *)outFile.c_str();
			if(execvp(cmd,arguments) == -1){// throw them in the execvp with command at front arguements in array
					cout << "ERROR: execvp fail on attempt, double check your command and arguements" << " - " << getpid() << endl;
					exit(-1);
			}
			} if(pid > 0 ) { //parent process
			pTotal++;
			//cout << pTotal << " - " << pid << endl;
			}
			children++;
			if(children >= procs){
				break;
			}
	 	}
	 }
		//cout << "done or didnt work" << endl;

	return 0		;
}
